<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

if (isset($_POST['idpagar'])) {
    $idpagar = (int) $_POST['idpagar'];
} else {
    $idpagar = (int) $_GET['idpagar'];
}

$sql = "Select descricao, valor, data_paga,num_parcela,entrada, usuario, idcliente From contaspagar Where idpagar = $idpagar";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);

if (!$registro) {
    javascriptAlertFim('Contas a pagar inexistente.', 'contas-pagar.php');
}

if ($_POST) {
    $descricao = $_POST['descricao'];
    $dtVencimento = $_POST['dtVencimento'];
    $data = date_converter($dtVencimento);
    //$dtVencimento = $_POST['dtVencimento'];

    $saldo = $_POST['valor'];
    $num_parcela = $_POST['num_parcela'];
    $idcliente = $_POST['cliente'];
    
  if (isset($_POST['ativo'])) {
    $entrada = COM_ENTRADA;
  }
  else {
    $entrada = SEM_ENTRADA;
  }

    if ($idcliente <= 0) {
        $msg[] = 'Selecione um cliente';
    } else {
        $sql = "SELECT * FROM cliente
    WHERE idcliente = $idcliente";
        $consulta = mysqli_query($con, $sql);
        $cli2 = mysqli_fetch_assoc($consulta);
        if (!$cli2) {
            $msg[] = 'Este cliente não existe';
        }
    }

    if ($descricao == '') {
        $msg[] = 'Informe a descrição';
    }
    if ($saldo == '') {
        $msg[] = 'Informe o saldo';
    }
    if ($data == '') {
        $msg[] = 'Informe a data de vencimento';
    }
    if ($num_parcela == '') {
        $msg[] = 'Informe o número da parcela';
    }
        if ($num_parcela < 0) {
        $msg[] = 'O número de parcela não pode ser menor que zero';
    }

    if (!$msg) {

        $sql = "UPDATE contaspagar SET "
                . "descricao='$descricao',valor='$saldo',data_paga='$data', num_parcela='$num_parcela', entrada='$entrada', idcliente= '$idcliente' WHERE idpagar = $idpagar";

        $update = mysqli_query($con, $sql);
        if (!$update) {
            $msg[] = 'Falha para alterar a conta ';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {
            javascriptAlertFim('Conta a pagar foi atualizado', 'contas-pagar.php');
        }
    }
} else {

    $descricao = $registro['descricao'];
    $saldo = $registro['valor'];
    $data = $registro['data_paga'];
    $data1 = date("Y-m-d", strtotime(str_replace('/', '-', $data)));
    $data2 = implode('/', array_reverse(explode('-', $data1)));
    $num_parcela = $registro['num_parcela'];
    $idcliente = $registro['idcliente'];
     $entrada = $registro['entrada'];
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Editar conta</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-cubes"></i> Editar contas a pagar #<?php echo $idpagar; ?></h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="contas-pagar-editar.php">
                <input type="hidden" name="idpagar" value="<?php echo $idpagar; ?>">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fcliente">Cliente</label>
                            <select class="form-control" id="fcliente" name="cliente">
                                <option value="0">Selecione um cliente</option>
                                <?php
                                $sql = "Select idcliente, nome From cliente Order by nome";
                                $q = mysqli_query($con, $sql);
                                while ($cliente = mysqli_fetch_assoc($q)) {
                                    ?>        
                                    <option value="<?php echo $cliente['idcliente']; ?>"
                                            <?php if ($idcliente == $cliente['idcliente']) { ?> selected <?php } ?>
                                            ><?php echo $cliente['nome']; ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>  
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fdescricao">Descrição</label>
                            <input type="text" class="form-control" id="fdescricao" name="descricao" placeholder="Ex: Conta de luz" value="<?php echo $descricao; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fvalor">Valor</label>
                            <input type="number" pattern="[0-9]+$" class="form-control" id="fvalor" name="valor" placeholder="Ex: 1000.98" value="<?php echo $saldo; ?>">
                        </div>
                    </div> 
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fdtVencimento">Data de vencimento</label>
                            <input type="text" class="form-control" data-mask="00/00/0000" id="fdtVencimento" placeholder="Ex: dd/mm/YYYY" name="dtVencimento" value="<?php echo $data2; ?>">
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="fnum_parcela">Número de parcela</label>
                            <input type="number" pattern="[0-9]+$" min="0" class="form-control" id="fnum_parcela" placeholder="No. de parcela" name="num_parcela" value="<?php echo $num_parcela; ?>">
                        </div>
                    </div>
                    
            <div class="col-xs-12">
              <div class="checkbox">
                <label for="fativo">
                  <input type="checkbox" name="ativo" id="fativo"
                  <?php if ($entrada == COM_ENTRADA) { ?> checked<?php } ?>
                  > Clique aqui se deseja "COM ENTRADA"
                </label>
              </div>
            </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>

        <script src="./js/pagar-parcela.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/mask.min.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
<?php

require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

//print_r($_SESSION);exit;
$acao = 0;
if (isset($_GET['acao'])) {
    $acao = (int) $_GET['acao'];
} elseif (isset($_POST['acao'])) {
    $acao = (int) $_POST['acao'];
}

if (isset($_GET['idcompra'])) {
    $idcompra = (int) $_GET['idcompra'];
} elseif (isset($_POST['idcompra'])) {
    $idcompra = (int) $_POST['idcompra'];
}

if (isset($_GET['idcliente'])) {
    $idcliente = (int) $_GET['idcliente'];
} elseif (isset($_POST['idcliente'])) {
    $idcliente = (int) $_POST['idcliente'];
}
//print_r($idcliente);exit;

if ($acao == 1) {

    $sql2 = "Select idusuario,idcompra,data,situacao,idcliente From compra Where (idcompra= idcompra)";
    $consulta2 = mysqli_query($con, $sql2);
    $conta2 = mysqli_fetch_assoc($consulta2);
    $idcompra = $conta2['idcompra'];

    $idcompra = $_GET['idcompra'];

    $sql = "Select cp.*, c.* From contaspagar cp inner join compra c on cp.idcompra = c.idcompra Where (cp.idcompra= $idcompra)";
    $consulta = mysqli_query($con, $sql);
    $conta = mysqli_fetch_assoc($consulta);

    $data2 = $conta['data'];
    $valor2 = $conta['total'];
    $parcelas = $conta['num_parcela'];
    $idMovimento = $conta['idpagar'];
    $entrada = $conta['entrada'];
    $situacao_parcela = PARCELA_ABERTA;

    $sql = "SELECT sum(valor_movimento) as vlrmovimento, idmovimento, count(*)as contador FROM `contaspagarparcelas` WHERE idmovimento =$idMovimento group by idmovimento";
    $r = mysqli_query($con, $sql);
    $m = mysqli_fetch_assoc($r);

    if ($m['contador'] >= 1) {
        javascriptAlertFim('Olá amigo (a) ! \n Você já gerou este registro de parcela. ', 'compras.php');
    }
    
 
    
// Função round arredonda para duas casas decimais
    $valor = round($valor2, 2);  // R$ 100,00
// Número de parcelas
//$parcelas = 3;
// A compra tem entrada?
// Pegue do banco o último ID inserido da movimentação
//$idMovimento = 1; 
// Calcula o valor da parcela dividindo o total pelo número de parcelas

    $valorParcela = round($valor / $parcelas, 2);


// Se tiver entrada diminui o número de parcelas
    $qtd = $entrada ? $parcelas - 1 : $parcelas;

// Faz um loop com a quantidade de parcelas
    for ($i = ($entrada ? 0 : 1); $i <= $qtd; $i++) {
        // Se for última parcela e a soma das parcelas for diferente do valor da compra
        // ex: 100 / 3 == 33.33 logo 3 * 33.33 == 99.99
        // Então acrescenta a diferença na parcela, assim última parcela será 33.34
        if ($qtd == $i && round($valorParcela * $parcelas, 2) != $valor) {
            $valorParcela += $valor - ($valorParcela * $parcelas);
        }

        // Caso a variavel $entrada seja true
        // o valor $i na primeira parcela será 0
        // então 30 * 0 == 0
        // será adicionado 0 dias a data, ou seja, a primeira parcela
        // será a data atual

        $data = 30 * $i;

        // Hoje mais X dias
        // Parcela 1: 30 dias
        // Parcela 2: 60 dias
        // Parcela 3: 90 dias...
        $data = date('Y-m-d H:i:s', strtotime("+{$data} days"));
        // data
        $datamovi = date('Y-m-d');
        // mostra o numero da parcela   
        $numero_parcela = $i;
        
//    $sql = "SELECT sum(vlr_pago) as vlr_pago FROM amortizacao_pagar where idcompra =' $idcompra'";
//    $res2 = mysqli_query($con, $sql);
//    $reg2 = mysqli_fetch_assoc($res2);
//    $vlrPago = $reg2['vlr_pago'];

        $sql = "INSERT INTO contaspagarparcelas(idmovimento,data_movimento,vencimento_movimento, pagamento_movimento, valor_movimento, numero_parcela, situacao_parcela, data_pagamento_parcela)"
                . "VALUES ('$idMovimento','$data2','$data','0','$valorParcela','$numero_parcela','$situacao_parcela','$data')";
        $result = mysqli_query($con, $sql);
        $idparcela = mysqli_insert_id($con);
        //Salvar codigo da parcela em sessao
        $_SESSION['idparcela'] = $idparcela;
        //Redireciona gerar as parcelas.php
        header('location:gerar-contas-parcela.php', 'Parcela gerada com sucesso');
    }
}
?>

<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
$msgOk = array();
$msgAviso = array();
if (!isset($_SESSION['idencomenda'])) {
    header('location:encomendas.php');
    exit;
}
$idencomenda = $_SESSION['idencomenda'];
$sql = "Select
	e.idencomenda,
	e.data,
	c.nome clienteNome,
        c.cpfcnpj,
        c.rgie,
        c.celular,
        c.inTipo,
        c.telefone,
        c.endereco,
        c.cep,
        c.bairro,
        c.numero,
        c.email,
	u.nome usuarioNome
        From encomenda e
        Inner Join cliente c
	On (c.idcliente = e.idcliente)
        Inner Join usuario u
	On (u.idusuario = e.idusuario)
        Where
        (e.idencomenda = $idencomenda)
        And (e.situacao = " . ENCOMENDA_ABERTA . ")";
        $consulta = mysqli_query($con, $sql);
        $encomenda = mysqli_fetch_assoc($consulta);
        if (!$encomenda) {
         header('location:encomendas.php');
         exit;
        }
/*
  Valores para acao
  1 = Incluir produto na venda
  2 = Remover produto na venda
  3 = Alterar 
 * 
 */
$acao = 0;
if (isset($_GET['acao'])) {
    $acao = (int) $_GET['acao'];
} elseif (isset($_POST['acao'])) {
    $acao = (int) $_POST['acao'];
}
if ($acao == 1) {
    $idproduto = (int) $_POST['idproduto'];

    $sql = "Select * From produto Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);
    $produto = mysqli_fetch_assoc($consulta);
    $precoProduto = $produto['precovenda'];
    $saldo = $produto['saldo'];
    $precoPago = $_POST['precopago'];
    $qtd = $_POST['qtd'];
    
        //$qtd vem do post digitado pelo usuario e compara com o saldo da tabela de produto
    if ($qtd >= $saldo) {
        $msgAviso[] = " Você tentou adicionar uma quantidade superior comparado ao saldo disponível ";
    } else if ($precoPago < 0) {
        $msgAviso[] = 'Você tentou adicionar um valor negativo ';
    } else {
        if ($saldo >= 0) {
            //$sql = "INSERT INTO vendaitem (idproduto, idvenda, preco, precopago, qtd)VALUES($idproduto, $idvenda, $precoProduto, $precoPago, $qtd)";
            $sql = "INSERT INTO encomendaitem (idproduto, idencomenda, preco, precopago, qtd)VALUES($idproduto, $idencomenda, $precoProduto, $precoPago, $qtd)";
            $inserir = mysqli_query($con, $sql);
            //adiciona os produtos desejados no item da venda
            if ($inserir) {
                $msgOk[] = "Adicionado $qtd x " . $produto['produto'];
            } else {
                $msgAviso[] = "Erro para inserir o produto na venda: " . mysqli_error($con);
            }
        } else {
            $msgAviso[] = 'Não foi possível adicionar este produto pois está sem saldo';
        }

        // baixa saldo do produto
        if ($saldo <= 0) {
            $msgAviso[] = "Saldo insuficiente, verificar com o administrador ";
        } else {
            $saldoatual = $saldo - $qtd;
            $sql = "UPDATE produto SET saldo = '$saldoatual' WHERE idproduto = $idproduto";
            $resultado = mysqli_query($con, $sql);

            if (!$resultado) {
                $msg[] = 'Falha ao salvar o saldo!';
                $msg[] = mysqli_error($con);
            } else {
                $msgOk[] = "Saldo atualizado com sucesso !";
            }
        }
    }
}
if ($acao == 2) {
    $idproduto = (int) $_GET['idproduto'];
    
        //select da dos itens da venda
    $sql = "Select * From encomendaitem Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);
    $produto = mysqli_fetch_assoc($consulta);
    $qtd = $produto['qtd'];

    //select dos produtos
    $sql = "Select * From produto Where (idproduto = $idproduto)";
    $consul = mysqli_query($con, $sql);
    $prod = mysqli_fetch_assoc($consul);
    $saldo = $prod['saldo'];

    //aqui faz a magica para estornar o saldo
    $saldoestornar = $saldo + $qtd;
    
    $sql = "UPDATE produto SET saldo = '$saldoestornar' WHERE idproduto = $idproduto";
    $resultado = mysqli_query($con, $sql);

    if (!$resultado) {
        $msg[] = 'Falha ao estornar o saldo!';
        $msg[] = mysqli_error($con);
    } else {
        $msgOk[] = "Saldo foi estornado com sucesso ";
    }

    $sql = "Delete From encomendaitem Where (idproduto = $idproduto)";
    $consulta = mysqli_query($con, $sql);

    $msgOk[] = "Produto removido da encomenda";
}


if ($acao == 3) {
   
    //select que busca o total de item vendidona encomendaitem
    $sql2 = "select precopago, qtd, sum( precopago * qtd) as totalitemvenda from encomendaitem where idencomenda = $idencomenda";
    $consulta2 = mysqli_query($con, $sql2);
    $res = mysqli_fetch_assoc($consulta2);
    $totalitemvenda = $res['totalitemvenda']; 
    
    //aqui busca o total de pagamento no caixa_pagamento
    $sql = "select e.idencomenda, cp.pagamento_id, sum(cp.pagamento_total) as totalpagamento  from caixa_pagamento cp inner join encomenda e 
    on cp.idencomenda = e.idencomenda
    where e.idencomenda = $idencomenda";
    $consulta = mysqli_query($con, $sql);
    $r = mysqli_fetch_assoc($consulta);
    $totalpago = $r['totalpagamento'];



    
  $sql = "Select idusuario, nome, nivelusu From usuario
  Where idusuario = idusuario
    And (situacao = '" . USUARIO_ATIVO . "')";
    $consulta = mysqli_query($con, $sql);
    $usuario = mysqli_fetch_assoc($consulta);

    if ($usuario) {
        $_SESSION['logado'] = 1;
        $_SESSION['idusuario'] = $usuario['idusuario'];
        $_SESSION['nome'] = $usuario['nome'];
        $usuario = $_SESSION['nome'];
    }
    
    $total = '';
    $desconto = '';
    $pagamento = '';
    $pagamento_status = PAGAR_PAGO;
    
if ($_POST) {
    $total = $_POST['total'];
    $desconto = $_POST['desconto'];
    $pagamento = $_POST['pagamento'];
    $dtPagamento = $_POST['dtPagamento'];
    $data = $dtPagamento;
    $transacao = rand(1000, 1000000000);
    //$troco = $total - $desconto - $pagamento;
    
    if (is_numeric($total) && is_numeric($desconto) && is_numeric($pagamento)) {
       $troco = $total - $desconto - $pagamento;
       $troco = number_format($troco, 2,',','.');
    }else{ 
        $msgAviso[] = 'Atenção, valor não numérico encontrado';
    } 
    
    if ($total =='') {
        $msgAviso[] = 'Informe o total';
    }
    if ($desconto == '') {
        $msgAviso[] = 'Informe o desconto';
    }
    if ($pagamento == '') {
        $msgAviso[] = 'Informe o pagamento';
    }
    
    if($totalitemvenda == $totalpago){
         $msgAviso[] = 'Esta compra pode ser finalizada, pois o valor comprado e pago estão iguais';       
    }
      
    if (!$msgAviso) {
        $_SESSION['transacao'] = $transacao;
       $sql = "INSERT INTO caixa_pagamento(pagamento_total, pagamento_desconto, pagamento_dinheiro, pagamento_troco, pagamento_usuario, pagamento_transacao, pagamento_data,pagamento_status,idencomenda) "
         . "VALUES ('$total','$desconto','$pagamento','$troco','$usuario','$transacao','$data','$pagamento_status','$idencomenda')";       
        $r = mysqli_query($con, $sql);
        if (!$r) {
            $msgAviso[] = 'Erro para salvar o pagamento';
            $msgAviso[] = mysqli_error($con);
        } else {
            javascriptAlertFim('Pagamento realizado com sucesso \n Troco: R$ '.$troco.'', 'encomenda-produto.php');
            
        }
    } 
  }
}


?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Produtos da encomenda</title>

<?php headCss(); ?>
    </head>
    <body>

<?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-shopping-cart"></i> Andamento da encomenda #<?php echo $idencomenda; ?></h1>
            </div>

<?php if ($msgOk) {
    msgHtml($msgOk, 'success');
} ?>
            <?php if ($msgAviso) {
                msgHtml($msgAviso, 'warning');
            } ?>

            <form role="form" method="post" action="encomenda-produto.php">
                <input type="hidden" name="acao" value="1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Adicionar produto</h3>
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-8">
                                    <div class="form-group">
                                        <label for="fidproduto">Produto</label>
                                        <select id="fidproduto" name="idproduto" class="form-control" required>
                                            <option value="">Selecione um produto</option>
                                            <?php
                                            $sql = 'Select * From produto Where situacao=' . PRODUTO_ATIVO;
                                            $result = mysqli_query($con, $sql);
                                            while ($linha = mysqli_fetch_assoc($result)) {
                                                ?>
                                                <option value="<?php echo $linha['idproduto']; ?>"><?php echo $linha['produto']; ?> (R$ <?php echo number_format($linha['precovenda'], 2, ",", "."); ?>) ------------ Saldo: <?php echo $linha['saldo'].' peças' ;?></option>
<?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fqtd">Quantidade</label>
                                        <input type="number" class="form-control" id="fqtd" value="0" name="qtd" min="1" required>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <div class="form-group">
                                        <label for="fpreco">Preço unitário</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="fprecopago" name="precopago" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Inserir</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                    </div>
                </div>
            </form>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Item (s) adicionados na encomenda</h3>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Qtd.</th>
                            <th>Produto</th>
                            <th>Preço unitário</th>
                            <th>Preço total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "Select ei.idproduto,p.produto,ei.precopago,ei.qtd From encomendaitem ei
                                Inner Join produto p On (p.idproduto = ei.idproduto)
                                Where (ei.idencomenda = $idencomenda)";
                        
                        $consulta = mysqli_query($con, $sql);
                        $encomendaTotal = 0;

                        while ($produto = mysqli_fetch_assoc($consulta)) {
                            $total = $produto['qtd'] * $produto['precopago'];
                            $encomendaTotal += $total;
                            ?>
                            <tr>
                                <td><?php echo $produto['qtd']; ?></td>
                                <td><?php echo $produto['produto']; ?></td>
                                <td>R$ <?php echo number_format($produto['precopago'], 2, ',', '.'); ?></td>
                                <td>R$ <?php echo number_format($total, 2, ',', '.'); ?></td>
                                <td><a href="encomenda-produto.php?acao=2&idproduto=<?php echo $produto['idproduto']; ?>" title="Remover produto da encomenda"><i class="fa fa-times fa-lg"></i></a></td>
                            </tr>
<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2">Total da encomenda</th>
                            <th>R$ <?php echo number_format($encomendaTotal, 2, ',', '.'); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            
            
            <form role="form" method="post" action="encomenda-produto.php">
                <input type="hidden" name="acao" value="3">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Efetuar pagamento no caixa</h3>
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                   <div class="form-group">
                            <label for="ftotal">Total</label>
                            <input type="text" class="form-control" id="ftotal" name="total" placeholder="Ex: 400.85">
                        </div>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                   <div class="form-group">
                            <label for="fdesconto">Desconto em R$</label>
                            <input type="text" class="form-control" id="fdesconto" name="desconto" placeholder="Desconto">
                        </div> 
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                            <label for="fdata">Data pagamento</label>
                            <input type="date" class="form-control" id="fdtPagamento" name="dtPagamento" placeholder="dd/mm/YYYY">
                        </div>
                                </div>

                        <div class="col-xs-12 col-sm-3 col-md-3">
                                   <div class="form-group">
                            <label for="fpagamento">Pagamento em dinheiro</label>
                            <input type="text" class="form-control" id="fpagamento" name="pagamento" placeholder="Ex: 100.00">
                        </div>
                        </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Efetuar pagamento</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                    </div>
                </div>
            </form>            
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados do pagamento</h3>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Data</th>
                            <th>Usuário</th>
                            <th>Total R$</th>
                            <th>Desconto R$</th>
                            <th>Dinheiro R$</th>
                            <th>Troco R$</th>
                            <th>Situação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from caixa_pagamento where idencomenda = $idencomenda";
                      $pagamentoTotal =0;
                        $consulta = mysqli_query($con, $sql);
                        while ($linha = mysqli_fetch_assoc($consulta)) {
                            $total = $linha['pagamento_total'];
                            $pagamentoTotal += $total;
                            ?>
                            <tr>
                                <td><?php echo $linha['pagamento_id']; ?></td>
                                <td><?php echo date('d/m/Y',strtotime($linha['pagamento_data'])); ?></td>
                                <td><?php echo $linha['pagamento_usuario']; ?></td>
                                <td><?php echo $linha['pagamento_total']; ?></td>
                                <td><?php echo $linha['pagamento_desconto']; ?></td>
                                <td><?php echo $linha['pagamento_dinheiro']; ?></td>
                                <td><?php echo $linha['pagamento_troco']; ?></td>
                                <td>
                                            <?php if ($linha['pagamento_status'] == PAGAR_PAGO) { ?>
                                                <span class="label label-success">PG</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">PE</span>
                                            <?php } ?>
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2">Total do pagamento</th>
                            <th>R$ <?php echo number_format($pagamentoTotal, 2, ',', '.'); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>           
            
            <form class="form-horizontal" method="post" action="encomenda-fechar.php">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fechamento da encomenda</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="fidencomenda" class="col-sm-2 control-label">Código:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $idencomenda; ?></p>
                            </div>

                            <label for="fdata" class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo date('d/m/Y', strtotime($encomenda['data'])); ?></p>
                            </div>

                            <label for="ftotal" class="col-sm-2 control-label">Total:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">R$ <?php echo number_format($encomendaTotal, 2, ',', '.'); ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcliente" class="col-sm-2 control-label">Cliente:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['clienteNome']; ?></p>
                            </div>
                            <label for="fcpf" class="col-sm-2 control-label">Cpf/Cnpj:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['cpfcnpj']; ?></p>
                            </div>
                             <label for="frgie" class="col-sm-2 control-label">Rg/Ie:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['rgie']; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="finTipo" class="col-sm-2 control-label">Tipo:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"
                                <?php if ($encomenda['inTipo'] == PESSOA_FISICA) { ?>
                                       <span class="label label-success">Fisica</span>
                                       <?php } else { ?>
                                        <span class="label label-warning">Juridica</span>
                                    <?php } ?></p>
                            </div>
                            <label for="ftelefone" class="col-sm-2 control-label">Telefone:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['telefone']; ?></p>
                            </div>
                            <label for="fcelular" class="col-sm-2 control-label">Celular:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['celular']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fcelular" class="col-sm-2 control-label">CEP:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['cep']; ?></p>
                            </div>
                            <label for="fendereco" class="col-sm-2 control-label">Endereço:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['endereco']; ?></p>
                            </div>
                            <label for="fbairro" class="col-sm-2 control-label">Bairro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['bairro']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fnumero" class="col-sm-2 control-label">Número:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['numero']; ?></p>
                            </div>
                             <label for="femail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['email']; ?></p>
                            </div>
                             <label for="fvendedor" class="col-sm-2 control-label">Vendedor:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $encomenda['usuarioNome']; ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">Fechar encomenda</button>
                        <p class="form-control-static pull-right"><strong>TOTAL:</strong> R$ <?php echo number_format($encomendaTotal, 2, ',', '.'); ?></p>
                    </div>
                </div>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

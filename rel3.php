<?php
require './protege.php';
require './config.php';     
require './lib/funcoes.php';
require './lib/conexao.php';
$msg = array();

$situacao = isset($_GET['situacao']);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Estoque de produtos</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="page-header">
                <h1><i class="fa fa-reorder"></i> Estoque de produtos</h1>
            </div>

            <?php if ($msg) { msgHtml($msg); } ?>

            <form role="form" method="get" action="rel3-imprimir.php">
                <div class="row">
                    <div class="form-group col-sm-6 col-xs-6">
                                         
                            <div class="form-group">
                                <label for="fsituacao">Situação</label>
                                <select class="form-control" name="situacao" id="situacao">
                                    <option value="">Selecione</option>
                                     <option value="0" <?php echo $situacao == PRODUTO_INATIVO ? 'selected' : ''; ?>>Inativo</option>
                                    <option value="1" <?php echo $situacao == PRODUTO_ATIVO ? 'selected' : ''; ?>>Ativo</option>
                                   
                                </select>
                            </div>
                        
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Salvar</button>
                <button type="reset" class="btn btn-danger">Cancelar</button>
            </form>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
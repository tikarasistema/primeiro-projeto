<?php
require './protege.php';
require './config.php';
require './lib/conexao.php';
require './lib/funcoes.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Clientes</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-user"></i> Clientes</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Clientes</h3>
                        </div>

                        <?php
                        $q = '';
                        if (isset($_GET['q'])) {
                            $q = trim($_GET['q']);
                        }
                        ?>
                        <form class="panel-body form-inline" role="form" method="get" action="">
                            <div class="form-group">
                                <label class="sr-only" for="fq">Pesquisa</label>
                                <input type="search" class="form-control" id="fq" name="q" placeholder="Pesquisa" value="<?php echo $q; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">Pesquisar</button>
                        </form>
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome/Razão social</th>
                                    <th>Status</th>
                                     <th>Tipo</th>
                                    <th>Cpf/Cnpj</th>
                                    <th>Cidade</th>
                                    <th>Telefone</th>
<!--                                    <th>E-mail</th>-->
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                                if (!$q) {

                                    $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                                    //seleciona todos os itens da tabela 
                                    $cmd = "Select
                                       c.idcliente, c.nome, c.email,c.cpfcnpj,c.inTipo,c.email,c.telefone, c.situacao, cid.cidade, cid.uf
                                       From cliente c Inner Join cidade cid On cid.idcidade = c.idcidade ";
                                    $clientes = mysqli_query($con, $cmd);
                                    //conta o total de itens 
                                    $total = mysqli_num_rows($clientes);
                                    //seta a quantidade de itens por página, neste caso, 2 itens 
                                    $registros = 5;
                                    //calcula o número de páginas arredondando o resultado para cima 
                                    $numPaginas = ceil($total / $registros);
                                    //variavel para calcular o início da visualização com base na página atual 
                                    $inicio = ($registros * $pagina) - $registros;
                                    //seleciona os itens por página 
                                    $cmd = "Select
                                       c.idcliente, c.nome, c.email,c.inTipo,c.cpfcnpj,c.email,c.telefone, c.situacao, cid.cidade, cid.uf
                                       From cliente c Inner Join cidade cid On cid.idcidade = c.idcidade limit $inicio,$registros";
                                    $clientes = mysqli_query($con, $cmd);
                                    $total = mysqli_num_rows($clientes);
                                    //exibe os produtos selecionados 
                                } else {
                                    $sql = "Select
                                       c.idcliente, c.nome, c.email,c.cpfcnpj,c.inTipo,c.email,c.telefone, c.situacao, cid.cidade, cid.uf
                                       From cliente c Inner Join cidade cid On cid.idcidade = c.idcidade";

                                    if ($q != '') {
                                        $sql .= " Where (c.nome like '%$q%')or (c.email like '%$q%')or (cid.cidade like '%$q%')";
                                    }
                                    $clientes = mysqli_query($con, $sql);
                                }

                                while ($linha = mysqli_fetch_assoc($clientes)) {
                                    //print_r($linha);exit;
                                    ?>
                                    <tr>
                                        <td><?php echo $linha['idcliente']; ?></td>
                                        <td><?php echo $linha['nome']; ?></td>
                                        <td>
                                            <?php if ($linha['situacao'] == CATEGORIA_ATIVO) { ?>
                                                <span class="label label-success">ativo</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">inativo</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ($linha['inTipo'] == PESSOA_FISICA) { ?>
                                                <span class="label label-success">Fisica</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">Juridica</span>
                                            <?php } ?>
                                        </td>

                                        <td><?php echo $linha['cpfcnpj']; ?></td>
                                         <td><?php echo $linha['cidade']; ?>&lt;<?php echo $linha['uf']; ?>&gt;</td>
                                        <td><?php echo $linha['telefone']; ?></td>
<!--                                        <td><?php //echo $linha['email']; ?></td>-->
                                        <td>
                                            <a href="clientes-editar.php?idcliente=<?php echo $linha['idcliente']; ?>" title="Editar"><i class="fa fa-edit fa-lg"></i></a>
                                            <a href="clientes-apagar.php?idcliente=<?php echo $linha['idcliente']; ?>" title="Remover"><i class="fa fa-times fa-lg"></i></a>
                                            <a href="venda-nova.php?idcliente=<?php echo $linha['idcliente']; ?>" title="Nova Venda"><i class="fa fa-credit-card"></i></a>
                                            <a href="compra-nova.php?idcliente=<?php echo $linha['idcliente']; ?>" title="Nova Compra"><i class="fa fa-truck"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p>Página:</p>
                    <?php
                    //exibe a paginação
                       if (!$q) {
                        if ($pagina > 1) {
                            echo "<a href='clientes.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='clientes.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='clientes.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
                    ?>
                </div>
            </div>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/clientes.css"/>

    </body>
</html>
<?php

require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

//print_r($_SESSION);exit;
$acao = 0;
if (isset($_GET['acao'])) {
    $acao = (int) $_GET['acao'];
} elseif (isset($_POST['acao'])) {
    $acao = (int) $_POST['acao'];
}

if (isset($_GET['idcompra'])) {
    $idcompra = (int) $_GET['idcompra'];
} elseif (isset($_POST['idcompra'])) {
    $idcompra = (int) $_POST['idcompra'];
}

if (isset($_GET['idparcela'])) {
    $idparcela = (int) $_GET['idparcela'];
} elseif (isset($_POST['idparcela'])) {
    $idparcela = (int) $_POST['idparcela'];
}
//print_r($idcliente);exit;

if ($acao == 1) {
    

$sql = "Select idparcela, data_movimento, vencimento_movimento, pagamento_movimento, valor_movimento, numero_parcela, situacao_parcela,data_pagamento_parcela, total_pago From contaspagarparcelas Where idparcela = $idparcela";
$resultado = mysqli_query($con, $sql);
$registro = mysqli_fetch_assoc($resultado);
$valor_movimento = $registro['valor_movimento'];
$total_pagamento_parcela = $registro['total_pago'];


if (!$registro) {
    javascriptAlertFim('Parcela inexistente.', 'gerar-contas-parcela.php');
}    
    
    // traz a soma do valor da parcela da tabela amortizacao
    $sql = "SELECT sum(vlr_pago) as vlr_pago FROM amortizacao_pagar where idparcela = $idparcela";
    $res2 = mysqli_query($con, $sql);
    $result = mysqli_fetch_assoc($res2);
    if (!$result) {
        $msg[] = 'Falha ao atualizar a parcela ';
        $msg[] = mysqli_error($con);
        $msg[] = $sql;
    } else {
        $vlrPago = $result['vlr_pago'];
    }


    $sql2 = "Select idusuario,idcompra,data,situacao,idcliente From compra Where (idcompra= idcompra)";
    $consulta2 = mysqli_query($con, $sql2);
    $conta2 = mysqli_fetch_assoc($consulta2);
    $idcompra = $conta2['idcompra'];

    $idcompra = $_GET['idcompra'];


 
     if ($total_pagamento_parcela == $valor_movimento) {
        $situacao_parcela = PARCELA_BAIXADA;
        $sql = "UPDATE contaspagarparcelas SET "
                . "situacao_parcela='$situacao_parcela' WHERE idparcela = $idparcela";

        $update = mysqli_query($con, $sql);
        if (!$update) {
            $msg[] = 'Falha na alteração ';
            $msg[] = mysqli_error($con);
            $msg[] = $sql;
        } else {
            javascriptAlertFim('O status da parcela foi atualizada para "BAIXADA"', 'gerar-contas-parcela.php');
        }
    }else{
        header("location: gerar-contas-parcela.php");   
    } 
}
?>

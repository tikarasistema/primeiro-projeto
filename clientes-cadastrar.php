<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';

$msg = array();

$nome = '';
$email = '';
$cpfcnpj = '';
$situacao = CLIENTE_ATIVO;
$rgie = '';
$idcidade = 0;
$telefone = '';
$celular = '';
$inTipo = '';
$cep = '';
$endereco = '';
$bairro = '';
$numero = '';


if ($_POST) {
    $nome = $_POST['cliente'];
    $email = $_POST['email'];
    $cpfcnpj = $_POST['cpfcnpj'];
    $rgie = $_POST['rgie'];
    $inTipo = $_POST['inTipo'];
    $idcidade = (int) $_POST['cidade'];
    $telefone = $_POST['telefone'];
    $celular = $_POST['celular'];
    $cep = (int) $_POST['cep'];
    $endereco = $_POST['endereco'];
    $bairro = $_POST['bairro'];
    $numero = $_POST['numero'];
    
      if ($idcidade <= 0) {
        $msg[] = 'Selecione uma cidade';
    } else {
        $sql = "SELECT * FROM cidade WHERE idcidade = $idcidade";
        $consulta = mysqli_query($con, $sql);
        $cidade2 = mysqli_fetch_assoc($consulta);
        if (!$cidade2) {
            $msg[] = 'Esta cidade não existe!';
        }
    }

    if ($nome == '') {
        $msg[] = 'Informe o nome do cliente';
    }
    if ($email == '') {
        $msg[] = 'Informe o email';
    }
    if ($cpfcnpj == '') {
        $msg[] = 'Informe o CPF/CNPJ';
    }
     if ($rgie == '') {
        $msg[] = 'Informe o RG/IE';
    }
    if ($telefone == '') {
        $msg[] = 'Informe o telefone';
    }
    if ($celular == '') {
        $msg[] = 'Informe o celular';
    }
    if ($inTipo == '') {
        $msg[] = 'Informe o tipo de situação';
    }
    
    if ($cep == '') {
        $msg[] = 'Informe o CEP';
    }
    if ($endereco == '') {
        $msg[] = 'Informe o endereço';
    }
    if ($bairro == '') {
        $msg[] = 'Informe o bairro';
    }
    if ($numero == '') {
        $msg[] = 'Informe o número';
    }
//    
//            if ($inTipo == PESSOA_FISICA) {
//            $retorno = cpf($cpfcnpj);
//            if (true !== $retorno) {
//                addMessage('CPF inválido!');
//                erro();
//            }
//        } elseif ($inTipo == PESSOA_JURIDICA) {
//            $retorno = cnpj($cpfcnpj);
//            if (!$retorno) {
//                addMessage('CNPJ inválido!');
//                erro();
//            }
//        }

    $sql = "INSERT INTO cliente(nome, email, situacao, idcidade, cpfcnpj, rgie, telefone, celular, inTipo, cep, endereco, bairro, numero) "
            . "VALUES ('$nome', '$email', '$situacao', '$idcidade', '$cpfcnpj', '$rgie', '$telefone', '$celular', '$inTipo', '$cep', '$endereco', '$bairro', '$numero')";
    
    
    $r = mysqli_query($con, $sql);

    if (!$r) {
        $msg[] = 'Erro para salvar';
        $msg[] = mysqli_error($con);
    } else {
        javascriptAlertFim('Registro foi salvo com sucesso !', 'clientes.php');
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cadastrar cliente</title>
        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-user"></i> Cadastrar cliente</h1>
                    </div>
                </div>
            </div>

            <?php
            if ($msg) {
                msgHtml($msg);
            }
            ?>

            <form class="row" role="form" method="post" action="clientes-cadastrar.php">
                <div class="col-xs-12">

                    <div class="row">                               
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="ftipo">Tipo*</label>
                                <select class="form-control" name="inTipo" id="finTipo">
                                    <option value="0">Selecione</option>
                                    <option value="1" <?php echo $inTipo == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                    <option value="2" <?php echo $inTipo == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcliente">Nome/Razão social </label>
                                <input type="text" class="form-control" id="fcliente" name="cliente" placeholder="Nome completo" value="<?php echo $nome; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcpf">CPF/CNPJ</label>
                                <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Digite somente números" size="40" maxlength="18" onBlur="validaFormato(this);" onkeypress="return (apenasNumeros(event))" value="<?php echo $cpfcnpj; ?>">
                            <div id="divResultado"></div>
                            </div>
                        </div>
                            <div class="col-xs-6">
                            <div class="form-group">
                                <label for="frgie">RG/IE</label>
                                <input type="text" class="form-control" id="frgie" name="rgie" maxlength="15" placeholder="Digite somente números"value="<?php echo $rgie; ?>">
                            <div id="divResultado"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="ftelefone">Telefone</label>
                                <input type="text" class="form-control" data-mask="(00) 0000-0000" id="ftelefone" name="telefone" placeholder="(00) 0000-0000" maxlength="" value="<?php echo $telefone; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcelular">Celular</label>
                                <input type="text" class="form-control" data-mask="(00) 0000-0000" id="fcelular" name="celular" placeholder="(00) 00000-0000" value="<?php echo $celular; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="femail">Email</label>
                                <input type="text" class="form-control" id="femail" name="email"  placeholder="Ex: email@email.com" value="<?php echo $email; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fcidade">Cidade</label>
                                <select class="form-control" id="fcidade" name="cidade">
                                    <option value="0">Selecione uma cidade</option>
                                    <?php
                                    $sql = "Select idcidade, cidade, uf From cidade Order By cidade";
                                    $q = mysqli_query($con, $sql);
                                    while ($cidade = mysqli_fetch_assoc($q)) {
                                        ?>        
                                        <option value="<?php echo $cidade['idcidade']; ?>"
                                                <?php if ($idcidade == $cidade['idcidade']) { ?> selected <?php } ?>
                                                ><?php echo $cidade['cidade']; ?>/<?php echo $cidade['uf']; ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fendereco">Endereço</label>
                                <input type="text" class="form-control" id="fendereco" name="endereco" placeholder="Ex: Rua raposo tavares, zona 01" value="<?php echo $endereco; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="fbairro">Bairro</label>
                                <input type="text" class="form-control" id="fbairro" name="bairro" placeholder="Ex: Zona 01" value="<?php echo $bairro; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="fnumero">Número</label>
                                <input type="text" class="form-control" id="fnumero" name="numero" placeholder="Número" value="<?php echo $numero; ?>">
                            </div>
                        </div>   
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for="fcep">CEP</label>
                                <input type="text" class="form-control" maxlength="8" pattern= "\d{5}-?\d{3}" id="fcep" name="cep" placeholder="Ex: xxxxx-xxx" value="<?php echo $cep; ?>">
                            </div>
                        </div>                      
                    </div>
                     <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label for="fativo">
                                <input type="checkbox" name="ativo" id="fativo"
                                       <?php if ($situacao == CLIENTE_ATIVO) { ?> checked<?php } ?>
                                       > Cliente ativo
                            </label>
                        </div>
                    </div>  
                     </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <script src="./js/cliente.js"></script>
        <script src="./lib/jquery.js"></script>
        <script src="./lib/mask.min.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
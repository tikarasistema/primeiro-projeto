<?php
require './protege.php';
require './config.php';
require './lib/funcoes.php';
require './lib/conexao.php';
//pegar id cliente
$idcliente = $_GET['idcliente'];
//pegar id usuario
$idusuario = $_SESSION['idusuario'];
// Verificar se existe uma venda aberta para $idcliente
// Se existir não abrir outra venda
$sql = "Select idcompra From compra Where (idcliente = $idcliente) And (status = " . COMPRA_ABERTA . ")";
$consulta = mysqli_query($con, $sql);
$compra = mysqli_fetch_assoc($consulta);
if ($compra) {
  // Existe outra venda
  header('location:compra-continuar.php?idcompra=' . $compra['idcompra']);
  exit;
}
// Criar uma venda
//Criar um registro na tabela venda
$data = date('Y-m-d');
$situacao = COMPRA_ABERTA;
$sql = "Insert into compra
(data, idcliente, situacao, idusuario)
Values
('$data', $idcliente, $situacao, $idusuario)";
$result = mysqli_query($con, $sql);
//Pegar o codigo da venda
$idcompra = mysqli_insert_id($con);
//Salvar codigo da venda em sessao
$_SESSION['idcompra'] = $idcompra;
//Redirecionar usuario para venda-produto.php
header('location:compra-produto.php');
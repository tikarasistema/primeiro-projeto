<?php
require './protege.php';
require './config.php';
require './lib/conexao.php';
require './lib/funcoes.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Produtos</title>

        <?php headCss(); ?>
    </head>
    <body>

        <?php include 'nav.php'; ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-header">
                        <h1><i class="fa fa-barcode"></i> Produtos</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Produtos</h3>
                        </div>

                        <?php
                        $idcategoria = 0;
                        $q = '';

                        if (isset($_GET['idcategoria'])) {
                            $idcategoria = (int) $_GET['idcategoria'];
                        }
                        if (isset($_GET['q'])) {
                            $q = trim($_GET['q']);
                        }
                        ?>
                        <form class="panel-body form-inline" role="form" method="get" action="">
                            <div class="form-group">
                                <label class="sr-only" for="fq">Pesquisa</label>
                                <input type="search" class="form-control" id="fq" name="q" placeholder="Pesquisa" value="<?php echo $q; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">Pesquisar</button>
                        </form>

                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Categoria</th>
                                    <th>Produto</th>
                                    <th>Saldo</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>


                                <?php
                                //verifica a página atual caso seja informada na URL, senão atribui como 1ª página 
                                if (!$q) {

                                    $pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
                                    //seleciona todos os itens da tabela 
                                    $cmd = "SELECT
                                    p.idproduto, p.idcategoria,
                                    c.categoria,
                                    p.saldo,
                                    p.produto, p.situacao
                                    FROM produto p
                                    Inner Join categoria c On (c.idcategoria = p.idcategoria)
                                    ";
                                    $produtos = mysqli_query($con, $cmd);
                                    //conta o total de itens 
                                    $total = mysqli_num_rows($produtos);
            
                                    //seta a quantidade de itens por página, neste caso, 2 itens 
                                    $registros = 5;
                                    //calcula o número de páginas arredondando o resultado para cima 
                                    $numPaginas = ceil($total / $registros);
                                    //variavel para calcular o início da visualização com base na página atual 
                                    $inicio = ($registros * $pagina) - $registros;
                                    //seleciona os itens por página 
                                    $cmd = "SELECT
                                    p.idproduto, p.idcategoria,
                                    c.categoria,
                                    p.saldo,
                                    p.produto, p.situacao
                                    FROM produto p
                                    Inner Join categoria c On (c.idcategoria = p.idcategoria)
                                    limit $inicio,$registros";
                                    $produtos = mysqli_query($con, $cmd);
                                    $total = mysqli_num_rows($produtos);
                                   
                                    //exibe os produtos selecionados 
                                } else {
                                    $sql = "SELECT
                                    p.idproduto, p.idcategoria,
                                    c.categoria,
                                    p.saldo,
                                    p.produto, p.situacao
                                    FROM produto p
                                    Inner Join categoria c On (c.idcategoria = p.idcategoria)";
                                 if ($q != '') {
                                        $sql .= " Where (c.categoria like '%$q%')or (p.produto like '%$q%')";
                                    }

                                    $produtos = mysqli_query($con, $sql);
                                }
                                while ($linha = mysqli_fetch_assoc($produtos)) {
                                    //print_r($linha);exit;
                                    ?>
                                    <tr>
                                        <td><?php echo $linha['idproduto']; ?></td>
                                        <td>
                                            <?php if ($linha['situacao'] == PRODUTO_ATIVO) { ?>
                                                <span class="label label-success">ativo</span>
                                            <?php } elseif ($linha['situacao'] == PRODUTO_MANUTENCAO) { ?>
                                                <span class="label label-info">manutenção</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">inativo</span>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $linha['categoria']; ?></td>
                                        <td><?php echo $linha['produto']; ?></td>
                                        <td><?php echo $linha['saldo']; ?></td>
                                        <td>
                                            <a href="produtos-editar.php?idproduto=<?php echo $linha['idproduto']; ?>" title="Editar"><i class="fa fa-edit fa-lg"></i></a>
                                            <a href="produtos-apagar.php?idproduto=<?php echo $linha['idproduto']; ?>" title="Remover"><i class="fa fa-times fa-lg"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p>Página:</p>
                    <?php
                    //exibe a paginação
                       if (!$q) {
                        if ($pagina > 1) {
                            echo "<a href='produtos.php?pagina=" . ($pagina - 1) . "' class='controle'>&laquo; anterior</a>";
                        }

                        for ($i = 1; $i < $numPaginas + 1; $i++) {
                            $ativo = ($i == $pagina) ? 'numativo' : '';
                            echo "<a href='produtos.php?pagina=" . $i . "' class='numero " . $ativo . "'> " . $i . " </a>";
                        }
                        if ($pagina < $numPaginas) {
                            echo "<a href='produtos.php?pagina=" . ($pagina + 1) . "' class='controle'>proximo &raquo;</a>";
                        }
                    }
                    ?>
                </div>
            </div>

        </div>

        <script src="./lib/jquery.js"></script>
        <script src="./lib/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/produtos.css"/>

    </body>
</html>
